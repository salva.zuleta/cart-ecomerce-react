// Cuenta los duplicados (para saber cuantos productos completamente iguales tenemos en el carrito)
export const countDuplicatesItemArray = (value, array) => {
	let count = 0;
	array.forEach(arrayValue => {
		if(arrayValue == value) {
			count++;
		}
	});
	return count;
};

// esto es para saber cuantos productos unicos hay en el carrito
export const removeArrayDuplicates = array => {
  return Array.from(new Set(array));
};

//Este sirve para bajar las cantidades de productos en el carrito
export const removeItemArray = (array, item) => {
	const index = array.indexOf(item);

	if(index > -1) {
		array.splice(index, 1);

	}
	return array;
};